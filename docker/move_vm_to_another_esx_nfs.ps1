
$dest_host = (Get-ChildItem ENV:VI_SERVER_DEST).Value
$src_host = (Get-ChildItem ENV:VI_SERVER_SRC).Value
$src_host_login = (Get-ChildItem ENV:VI_USERNAME).Value
$src_host_password = (Get-ChildItem ENV:VI_PASSWORD).Value
$vmname = (Get-ChildItem ENV:VI_VM).Value

Write-Host -ForegroundColor magenta "DOCKER ENV VARS ..."
Write-Host " INFO - VI_SERVER=$VI_SERVER"
Write-Host " INFO - VI_USERNAME=$VI_USERNAME"
Write-Host " INFO - VI_VM=$VI_VM"
Write-Host " INFO - VI_VLANDEST=$VI_VLANDEST"



Try {

    Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false | Out-Null
    Write-Host "======================================================="
    Write-Host "   START TASK: MOVE MSH"
    Write-Host "======================================================="

    # CONNECT TO HOST SOURCE
    $src_connect = Connect-VIServer -Server $src_host -User $src_host_login -Password $src_host_password -Force

    # CONNECT TO HOST DESTINATION
    $dest_connect = Connect-VIServer -Server $dest_host -User $src_host_login -Password $src_host_password -Force

    # RETRIEVE VM TO SOURCE
    $src_vm = get-vm -name $vmname -ErrorAction SilentlyContinue
    if (-Not $src_vm ) {
        Write-Host "ERROR - CANNOT RETRIEVE VM: " $vmname
        return(1)
    }

    # Retrieve params
    $vmDatastoreIdList = $src_vm.DatastoreIdList
    $nfs_name =  $nfs_path -replace("/vim/backup/", "BACKUP-")
    $nfs_path =  $vmDatastoreIdList.Split(":")[1]
    $nfs_host =  $vmDatastoreIdList.Split(":")[0] -replace("Datastore-", "")

    # MOUNT NFS TO DEST
    Write-Host "MOUNTING NFS DEST"
    $newDatastore = New-Datastore -VMHost $dest_host -Nfs -Name $nfs_name -Path $nfs_path -NfsHost $nfs_host -Server $dest_connect -ErrorAction SilentlyContinue -ErrorVariable $errvar
    if (-Not $newDatastore) {
        Write-Host "ERROR - CANNOT MOUNT NFS: " $errvar
        return(1)
    }

    # Inventoring DEST VM
    Write-Host "INVENTORING VM DEST"
    $CreatedVm = New-VM -VMFilePath $src_vm.ExtensionData.Config.Files.VmPathName -VMHost $(Get-VMHost -Server $dest_connect) -Name $vmname -ErrorAction SilentlyContinue
    if($CreatedVm -is [VMware.VimAutomation.ViCore.Types.V1.Inventory.VirtualMachine]) {
      Write-Host $vmname" : Inventoried with success"

    } else {
      Write-Host "ERROR - " $vmname" : Inventory failure"
      return(1)
    }

    # Removing SOURCE VM
    Write-Host "Removing $($vmname)"
    Remove-VM $vmname -Confirm:$false -ErrorVariable $err -Server $src_connect
    if ($err) {
        Write-Host "ERROR - Removing $($vmname) : failed"
        return(1)
    } else {
        Write-Host "Removing $($vmname) : Ok"
    }

    # Start REMOVE
    Remove-Datastore -VMHost $(Get-VMHost -Server $src_connect) -Datastore $(Get-Datastore -Name $nfs_name -Server $src_connect) -confirm:$false -ErrorAction SilentlyContinue -ErrorVariable $checkrmSource
    if($checkrmSource) {
        Write-Host  "ERROR - Oopy, cannot remove nfs datastore to source"
        return(1)
    } else {
        Write-Host  "Datastore NFS is removed to source"
    }

    # RETURN
    return(0)

} Catch {
    $ErrorMessage = $_.Exception.Message
    $FailedItem = $_.Exception.ItemName
    Write-Host "ERROR Exception: " $_.Exception

} Finally {
    Disconnect-VIServer -Server $src_connect -Force -Confirm:$False
    Disconnect-VIServer -Server $dest_connect -Force -Confirm:$False
    Write-Host "======================================================="
    Write-Host "   END TASK: MOVE MSH"
    Write-Host "======================================================="
}


