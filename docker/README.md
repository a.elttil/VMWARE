

## RUN SAMPLE convert_vm_to_template.ps1
docker run --rm -it \
--entrypoint='/usr/bin/powershell' \
-e VI_SERVER=vc-lab.alworms.com \
-e VI_USERNAME=razor \
-e VI_PASSWORD='XXXXXXXX' \
-e VI_VM="template002_ubuntu-1604-base" \
-e VI_VLANDEST="GARAGE-LAB" \
-v xxx/powercli:/scripts vmware/powerclicore /scripts/convert_vm_to_template.ps1


## RUN SCRIPT move_vm_to_another_esx_nfs.ps1
docker run --rm -it \
--entrypoint='/usr/bin/powershell' \
-e VI_SERVER_DEST='10.1.1.2' \
-e VI_SERVER_SRC='10.1.1.1' \
-e VI_USERNAME=razor \
-e VI_PASSWORD='XXXXXXXX' \
-e VI_VM="template002_ubuntu-1604-base" \
-v xxx/powercli:/scripts vmware/powerclicore /scripts/convert_vm_to_template.ps1

