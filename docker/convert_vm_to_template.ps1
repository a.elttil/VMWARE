


$VI_SERVER = (Get-ChildItem ENV:VI_SERVER).Value
$VI_USERNAME = (Get-ChildItem ENV:VI_USERNAME).Value
$VI_PASSWORD = (Get-ChildItem ENV:VI_PASSWORD).Value
$VI_VM = (Get-ChildItem ENV:VI_VM).Value
$VI_VLANDEST = (Get-ChildItem ENV:VI_VLANDEST).Value

Write-Host -ForegroundColor magenta "DOCKER ENV VARS ..."
Write-Host " INFO - VI_SERVER=$VI_SERVER"
Write-Host " INFO - VI_USERNAME=$VI_USERNAME"
Write-Host " INFO - VI_VM=$VI_VM"
Write-Host " INFO - VI_VLANDEST=$VI_VLANDEST"

$failed=$false

Try {


    Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false | Out-Null

    Write-Host "======================================================="
    Write-Host "   START TASK: SETUP TEMPLATE"
    Write-Host "======================================================="

    # CONNECT TO VCSA
    Write-Host "`nConnecting to vCenter Server ..."
    $connect = Connect-VIServer -Server $VI_SERVER -User $VI_USERNAME -Password $VI_PASSWORD -Force | Out-Null

    # RETRIEVE VM
    $vm = Get-VM -Name $VI_VM -ErrorAction SilentlyContinue
    if (-Not $vm) {
        Write-Host "ERROR - Cannot retrieve VM: " $VI_VM
        $failed=$true
        return
    } else {
        Write-Host "SUCCESS - Retrieve VM: " $VI_VM
    }

    # Get-NetworkAdapter
    $network = Get-NetworkAdapter -VM $vm -ErrorAction SilentlyContinue
    if (-Not $vm) {
        Write-Host "ERROR - Cannot retreive VM NetworkAdapter"
        $failed=$true
        return

    } else {
        Write-Host "SUCCESS - Retrieve VM NetworkAdapter " $network.name
    }

    # Set Vlan
    $networkadapter = Set-NetworkAdapter -NetworkAdapter $network -NetworkName $VI_VLANDEST -ErrorAction SilentlyContinue -Confirm:$false
    if (-Not $networkadapter) {
        Write-Host "ERROR - Cannot reconfigure vlan " $VI_VLANDEST " to vm " $VI_VM
        $failed=$true
        return
    } else {
        Write-Host "SUCCESS - Reconfigure vlan " $VI_VLANDEST " to vm " $VI_VM
    }

    # Init VirtualMachineConfigSpec object
    $spec = New-Object VMware.Vim.VirtualMachineConfigSpec
    $spec.vAppConfig = New-Object VMware.Vim.VmConfigSpec
    $spec.vAppConfig.property = New-Object VMware.Vim.VAppPropertySpec[] (20)

    # DNS
    $spec.vAppConfig.property[1] = New-Object VMware.Vim.VAppPropertySpec
    $spec.vAppConfig.property[1].operation = "add"
    $spec.vAppConfig.property[1].info = New-Object VMware.Vim.VAppPropertyInfo
    $spec.vAppConfig.property[1].info.key = 1
    $spec.vAppConfig.property[1].info.id = "dns0"
    $spec.vAppConfig.property[1].info.value = "8.8.8.8"

    # GATEWAY
    $spec.vAppConfig.property[2] = New-Object VMware.Vim.VAppPropertySpec
    $spec.vAppConfig.property[2].operation = "add"
    $spec.vAppConfig.property[2].info = New-Object VMware.Vim.VAppPropertyInfo
    $spec.vAppConfig.property[2].info.key = 2
    $spec.vAppConfig.property[2].info.id = "gateway"
    $spec.vAppConfig.property[2].info.value = ""

    # HOSTNAME
    $spec.vAppConfig.property[3] = New-Object VMware.Vim.VAppPropertySpec
    $spec.vAppConfig.property[3].operation = "add"
    $spec.vAppConfig.property[3].info = New-Object VMware.Vim.VAppPropertyInfo
    $spec.vAppConfig.property[3].info.key = 3
    $spec.vAppConfig.property[3].info.id = "hostname"
    $spec.vAppConfig.property[3].info.value = "server001"

    # IPADDRESS
    $spec.vAppConfig.property[4] = New-Object VMware.Vim.VAppPropertySpec
    $spec.vAppConfig.property[4].operation = "add"
    $spec.vAppConfig.property[4].info = New-Object VMware.Vim.VAppPropertyInfo
    $spec.vAppConfig.property[4].info.key = 4
    $spec.vAppConfig.property[4].info.id = "ip0"
    $spec.vAppConfig.property[4].info.value = ""

    # STATIC IP (TRUE/FALSE)
    $spec.vAppConfig.property[5] = New-Object VMware.Vim.VAppPropertySpec
    $spec.vAppConfig.property[5].operation = "add"
    $spec.vAppConfig.property[5].info = New-Object VMware.Vim.VAppPropertyInfo
    $spec.vAppConfig.property[5].info.key = 5
    $spec.vAppConfig.property[5].info.id = "ip_static"
    $spec.vAppConfig.property[5].info.value = $true

    # NETMASK
    $spec.vAppConfig.property[6] = New-Object VMware.Vim.VAppPropertySpec
    $spec.vAppConfig.property[6].operation = "add"
    $spec.vAppConfig.property[6].info = New-Object VMware.Vim.VAppPropertyInfo
    $spec.vAppConfig.property[6].info.key = 6
    $spec.vAppConfig.property[6].info.id = "netmask0"
    $spec.vAppConfig.property[6].info.value = ""

    # PASSWORD
    $spec.vAppConfig.property[7] = New-Object VMware.Vim.VAppPropertySpec
    $spec.vAppConfig.property[7].operation = "add"
    $spec.vAppConfig.property[7].info = New-Object VMware.Vim.VAppPropertyInfo
    $spec.vAppConfig.property[7].info.key = 7
    $spec.vAppConfig.property[7].info.id = "password"
    $spec.vAppConfig.property[7].info.value = ""

    # Enable guestInfo
    $spec.VAppConfig.OvfEnvironmentTransport = @('com.vmware.guestInfo')

    # Reconfigure VM
    Write-Host "SUCCESS - Reconfigure SPEC"
    $vm.ExtensionData.ReconfigVM_Task($spec)

    # Convert to template
    Write-Host "SUCCESS - CONVERT TO TEMPLATE"
    $vm | Set-VM -ToTemplate -Name $VI_VM -Confirm:$False -ErrorAction SilentlyContinue

} Catch {
    $ErrorMessage = $_.Exception.Message
    $FailedItem = $_.Exception.ItemName
    Write-Host "ERROR - " $_.Exception
    $failed=$true

} Finally {
    Write-Host "Disconnecting to vCenter Server ..."
    Disconnect-VIServer -Server $VI_SERVER -Force -Confirm:$False  | Out-Null

    Write-Host "======================================================="
    Write-Host "   END TASK: SETUP TEMPLATE"
    Write-Host "======================================================="

    if ($failed) {
        Exit 1
    } else {
        Exit 0
    }
}

